import {interfaces} from 'inversify';
import ServiceIdentifier = interfaces.ServiceIdentifier;
import Newable = interfaces.Newable;

export interface ProvideService {
    readonly provide: ServiceIdentifier<any>;
    readonly multi?: boolean;
    readonly class?: Newable<any>;
    readonly value?: any;
    readonly factory?: (...deps: any[]) => any;
    readonly deps?: ReadonlyArray<ServiceIdentifier<any>>;
}

export function isProvider(something: any): something is ProvideService {
    return something != null && something['provide'] != null;
}


export interface ProcessingModuleSpecProcessor {
    (moduleSpec: ModuleSpec): ReadonlyArray<ProvideService>;
}


export interface ModuleSpec {

}