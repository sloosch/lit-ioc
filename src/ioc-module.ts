import {ContainerModule, injectable, interfaces} from 'inversify';
import {ModuleSpec, ProcessingModuleSpecProcessor, ProvideService} from './types';
import Newable = interfaces.Newable;

const NOOP = () => {
};

const MODULE_MODULE_FACTORY = Symbol.for('MODULE_MODULE_FACTORY');


export function getContainerModule(processors: ReadonlyArray<ProcessingModuleSpecProcessor>, target: Newable<any>): ContainerModule | undefined {
    return (Reflect.getMetadata(MODULE_MODULE_FACTORY, target) || NOOP)(processors);
}

export function iocModule(moduleSpec: ModuleSpec) {
    return function (target: Newable<any>) {
        const moduleFactory = (processors: ReadonlyArray<ProcessingModuleSpecProcessor>) => new ContainerModule((bind: interfaces.Bind, unbind: interfaces.Unbind, isBound: interfaces.IsBound, rebind: interfaces.Rebind) => {
            const providers: ReadonlyArray<ProvideService> = processors.reduce((existing, fn) => existing.concat(fn(moduleSpec)), []);

            providers.forEach(service => {
                if (!service.multi && isBound(service.provide)) {
                    unbind(service.provide);
                }
                const bindingToSyntax = bind(service.provide);
                if (service.class) {
                    bindingToSyntax.to(service.class);
                } else if (service.factory) {
                    bindingToSyntax.toDynamicValue((context: interfaces.Context) => {
                        const deps = (service.deps || []).map(dep => context.container.get(dep));

                        return service.factory(...deps);
                    });
                } else if (service.value) {
                    bindingToSyntax.toConstantValue(service.value);
                } else {
                    bindingToSyntax.toSelf();
                }
            });
        });

        const injectableTarget = injectable()(target);
        Reflect.defineMetadata(MODULE_MODULE_FACTORY, moduleFactory, injectableTarget);

        return injectableTarget;
    };
}
