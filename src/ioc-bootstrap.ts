import {Container, interfaces} from 'inversify';
import {getContainerModule} from './ioc-module';
import {rootContainer} from './root-container';
import Newable = interfaces.Newable;
import {ProcessingModuleSpecProcessor} from './types';

export function iocBootstrap(modules: ReadonlyArray<Newable<any>>,
                             processors: ReadonlyArray<ProcessingModuleSpecProcessor>,
                             container: Container = rootContainer) {
    const containerModules = modules.map(module => getContainerModule(processors, module));

    containerModules.forEach((module, idx) => {
        if (!module) {
            throw new Error('"' + modules[idx] + '" does not describe a module. Missing module annotation?');
        }
    });
    container.load(...containerModules);

    modules.forEach(module => container.resolve(module));
}
