import {isProvider, ModuleSpec, ProvideService} from '../types';
import {interfaces} from 'inversify';
import Newable = interfaces.Newable;


declare module '../types' {
    export interface ModuleSpec {
        readonly services?: ReadonlyArray<Newable<any> | ProvideService>;
    }
}

export function servicesProcessor(moduleSpec: ModuleSpec): ReadonlyArray<ProvideService> {
    return (moduleSpec.services || [])
        .map(service => isProvider(service) ? service : {provide: service, multi: false});
}

