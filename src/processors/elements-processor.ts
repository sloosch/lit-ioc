import {ModuleSpec, ProvideService} from '../types';
import {ElementDefinition} from '../custom-elements.module';
export const MODULE_ELEMENTS = Symbol.for('MODULE_ELEMENTS');

declare module '../types' {
    export interface ModuleSpec {
        readonly elements?: ReadonlyArray<ElementDefinition>;
    }
}

export function elementsProcessor(moduleSpec: ModuleSpec): ReadonlyArray<ProvideService> {
    return (moduleSpec.elements || [])
        .map(element => ({
            provide: MODULE_ELEMENTS, value: element, multi: true
        }));
}
