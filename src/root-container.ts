import {Container} from 'inversify';
import getDecorators from "inversify-inject-decorators";

export const ROOT_CONTAINER = Symbol.for('ROOT_CONTAINER');

export const rootContainer = new Container({
    defaultScope: 'Singleton'
});

rootContainer.bind(ROOT_CONTAINER).toConstantValue(rootContainer);

const decorators = getDecorators(rootContainer);
export const lazyInject = decorators.lazyInject;
export const lazyInjectNamed = decorators.lazyInjectNamed;
export const lazyInjectTagged = decorators.lazyInjectTagged;
export const lazyMultiInject = decorators.lazyMultiInject;

