import {iocModule} from './ioc-module';
import {interfaces, multiInject, optional} from 'inversify';
import {MODULE_ELEMENTS} from './processors';
import Newable = interfaces.Newable;

export const CUSTOM_ELEMENT_REGISTRY = Symbol.for('CUSTOM_ELEMENT_REGISTRY');
export const ELEMENT_CONSUMER = Symbol.for('ELEMENT_CONSUMER');


export interface ElementConsumer {
    (element: ElementDefinition): void;
}

export interface ElementDefinition extends Newable<HTMLElement> {
    readonly is: string;
    readonly extends?: string;
}

@iocModule({
    services: [
        {provide: CUSTOM_ELEMENT_REGISTRY, value: window.customElements},
        {
            provide: ELEMENT_CONSUMER,
            deps: [CUSTOM_ELEMENT_REGISTRY],
            multi: true,
            factory: (customElementRegistry: CustomElementRegistry) => {
                return (element: ElementDefinition) => {
                    const options: ElementDefinitionOptions = {};
                    if(element.extends) {
                        options.extends = element.extends;
                    }
                    customElementRegistry.define(element.is, element, options);
                }
            }
        }
    ]
})
export class CustomElementsModule {
    constructor(@multiInject(ELEMENT_CONSUMER) consumers: ReadonlyArray<ElementConsumer>,
                @multiInject(MODULE_ELEMENTS) @optional() elements: ReadonlyArray<ElementDefinition>) {
        (elements || []).forEach(element => consumers.forEach(fn => fn(element)));
    }
}
