export { iocBootstrap } from './ioc-bootstrap';
export { iocModule } from './ioc-module';
export { ROOT_CONTAINER, rootContainer, lazyInject, lazyInjectNamed, lazyInjectTagged, lazyMultiInject } from './root-container';
export { CustomElementsModule, ElementDefinition, ElementConsumer, ELEMENT_CONSUMER, CUSTOM_ELEMENT_REGISTRY } from './custom-elements.module';
export { ProcessingModuleSpecProcessor, ProvideService, ModuleSpec } from './types';
