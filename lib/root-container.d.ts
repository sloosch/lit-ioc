import { Container } from 'inversify';
export declare const ROOT_CONTAINER: unique symbol;
export declare const rootContainer: Container;
export declare const lazyInject: (serviceIdentifier: string | symbol | import("inversify/dts/interfaces/interfaces").interfaces.Newable<any> | import("inversify/dts/interfaces/interfaces").interfaces.Abstract<any>) => (proto: any, key: string) => void;
export declare const lazyInjectNamed: (serviceIdentifier: string | symbol | import("inversify/dts/interfaces/interfaces").interfaces.Newable<any> | import("inversify/dts/interfaces/interfaces").interfaces.Abstract<any>, named: string) => (proto: any, key: string) => void;
export declare const lazyInjectTagged: (serviceIdentifier: string | symbol | import("inversify/dts/interfaces/interfaces").interfaces.Newable<any> | import("inversify/dts/interfaces/interfaces").interfaces.Abstract<any>, key: string, value: any) => (proto: any, propertyName: string) => void;
export declare const lazyMultiInject: (serviceIdentifier: string | symbol | import("inversify/dts/interfaces/interfaces").interfaces.Newable<any> | import("inversify/dts/interfaces/interfaces").interfaces.Abstract<any>) => (proto: any, key: string) => void;
