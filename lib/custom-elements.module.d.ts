import { interfaces } from 'inversify';
import Newable = interfaces.Newable;
export declare const CUSTOM_ELEMENT_REGISTRY: unique symbol;
export declare const ELEMENT_CONSUMER: unique symbol;
export interface ElementConsumer {
    (element: ElementDefinition): void;
}
export interface ElementDefinition extends Newable<HTMLElement> {
    readonly is: string;
    readonly extends?: string;
}
export declare class CustomElementsModule {
    constructor(consumers: ReadonlyArray<ElementConsumer>, elements: ReadonlyArray<ElementDefinition>);
}
