"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const ioc_module_1 = require("./ioc-module");
const inversify_1 = require("inversify");
const processors_1 = require("./processors");
exports.CUSTOM_ELEMENT_REGISTRY = Symbol.for('CUSTOM_ELEMENT_REGISTRY');
exports.ELEMENT_CONSUMER = Symbol.for('ELEMENT_CONSUMER');
let CustomElementsModule = class CustomElementsModule {
    constructor(consumers, elements) {
        (elements || []).forEach(element => consumers.forEach(fn => fn(element)));
    }
};
CustomElementsModule = __decorate([
    ioc_module_1.iocModule({
        services: [
            { provide: exports.CUSTOM_ELEMENT_REGISTRY, value: window.customElements },
            {
                provide: exports.ELEMENT_CONSUMER,
                deps: [exports.CUSTOM_ELEMENT_REGISTRY],
                multi: true,
                factory: (customElementRegistry) => {
                    return (element) => {
                        const options = {};
                        if (element.extends) {
                            options.extends = element.extends;
                        }
                        customElementRegistry.define(element.is, element, options);
                    };
                }
            }
        ]
    }),
    __param(0, inversify_1.multiInject(exports.ELEMENT_CONSUMER)),
    __param(1, inversify_1.multiInject(processors_1.MODULE_ELEMENTS)), __param(1, inversify_1.optional())
], CustomElementsModule);
exports.CustomElementsModule = CustomElementsModule;
