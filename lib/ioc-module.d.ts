import { ContainerModule, interfaces } from 'inversify';
import { ModuleSpec, ProcessingModuleSpecProcessor } from './types';
import Newable = interfaces.Newable;
export declare function getContainerModule(processors: ReadonlyArray<ProcessingModuleSpecProcessor>, target: Newable<any>): ContainerModule | undefined;
export declare function iocModule(moduleSpec: ModuleSpec): (target: interfaces.Newable<any>) => any;
