"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const inversify_1 = require("inversify");
const NOOP = () => {
};
const MODULE_MODULE_FACTORY = Symbol.for('MODULE_MODULE_FACTORY');
function getContainerModule(processors, target) {
    return (Reflect.getMetadata(MODULE_MODULE_FACTORY, target) || NOOP)(processors);
}
exports.getContainerModule = getContainerModule;
function iocModule(moduleSpec) {
    return function (target) {
        const moduleFactory = (processors) => new inversify_1.ContainerModule((bind, unbind, isBound, rebind) => {
            const providers = processors.reduce((existing, fn) => existing.concat(fn(moduleSpec)), []);
            providers.forEach(service => {
                if (!service.multi && isBound(service.provide)) {
                    unbind(service.provide);
                }
                const bindingToSyntax = bind(service.provide);
                if (service.class) {
                    bindingToSyntax.to(service.class);
                }
                else if (service.factory) {
                    bindingToSyntax.toDynamicValue((context) => {
                        const deps = (service.deps || []).map(dep => context.container.get(dep));
                        return service.factory(...deps);
                    });
                }
                else if (service.value) {
                    bindingToSyntax.toConstantValue(service.value);
                }
                else {
                    bindingToSyntax.toSelf();
                }
            });
        });
        const injectableTarget = inversify_1.injectable()(target);
        Reflect.defineMetadata(MODULE_MODULE_FACTORY, moduleFactory, injectableTarget);
        return injectableTarget;
    };
}
exports.iocModule = iocModule;
