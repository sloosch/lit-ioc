"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ioc_module_1 = require("./ioc-module");
const root_container_1 = require("./root-container");
function iocBootstrap(modules, processors, container = root_container_1.rootContainer) {
    const containerModules = modules.map(module => ioc_module_1.getContainerModule(processors, module));
    containerModules.forEach((module, idx) => {
        if (!module) {
            throw new Error('"' + modules[idx] + '" does not describe a module. Missing module annotation?');
        }
    });
    container.load(...containerModules);
    modules.forEach(module => container.resolve(module));
}
exports.iocBootstrap = iocBootstrap;
