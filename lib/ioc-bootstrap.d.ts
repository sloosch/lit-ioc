import { Container, interfaces } from 'inversify';
import Newable = interfaces.Newable;
import { ProcessingModuleSpecProcessor } from './types';
export declare function iocBootstrap(modules: ReadonlyArray<Newable<any>>, processors: ReadonlyArray<ProcessingModuleSpecProcessor>, container?: Container): void;
