"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const inversify_1 = require("inversify");
const inversify_inject_decorators_1 = require("inversify-inject-decorators");
exports.ROOT_CONTAINER = Symbol.for('ROOT_CONTAINER');
exports.rootContainer = new inversify_1.Container({
    defaultScope: 'Singleton'
});
exports.rootContainer.bind(exports.ROOT_CONTAINER).toConstantValue(exports.rootContainer);
const decorators = inversify_inject_decorators_1.default(exports.rootContainer);
exports.lazyInject = decorators.lazyInject;
exports.lazyInjectNamed = decorators.lazyInjectNamed;
exports.lazyInjectTagged = decorators.lazyInjectTagged;
exports.lazyMultiInject = decorators.lazyMultiInject;
