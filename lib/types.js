"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function isProvider(something) {
    return something != null && something['provide'] != null;
}
exports.isProvider = isProvider;
