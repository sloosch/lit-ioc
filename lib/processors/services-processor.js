"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const types_1 = require("../types");
function servicesProcessor(moduleSpec) {
    return (moduleSpec.services || [])
        .map(service => types_1.isProvider(service) ? service : { provide: service, multi: false });
}
exports.servicesProcessor = servicesProcessor;
