"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MODULE_ELEMENTS = Symbol.for('MODULE_ELEMENTS');
function elementsProcessor(moduleSpec) {
    return (moduleSpec.elements || [])
        .map(element => ({
        provide: exports.MODULE_ELEMENTS, value: element, multi: true
    }));
}
exports.elementsProcessor = elementsProcessor;
