import { ModuleSpec, ProvideService } from '../types';
import { interfaces } from 'inversify';
import Newable = interfaces.Newable;
declare module '../types' {
    interface ModuleSpec {
        readonly services?: ReadonlyArray<Newable<any> | ProvideService>;
    }
}
export declare function servicesProcessor(moduleSpec: ModuleSpec): ReadonlyArray<ProvideService>;
