import { ModuleSpec, ProvideService } from '../types';
import { ElementDefinition } from '../custom-elements.module';
export declare const MODULE_ELEMENTS: unique symbol;
declare module '../types' {
    interface ModuleSpec {
        readonly elements?: ReadonlyArray<ElementDefinition>;
    }
}
export declare function elementsProcessor(moduleSpec: ModuleSpec): ReadonlyArray<ProvideService>;
